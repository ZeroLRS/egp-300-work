// By Dan Buckstein
// Modified by: _______________________________________________________________
#include "egpfw/egpfw/egpfwFrameBuffer.h"


// OpenGL
#ifdef _WIN32
#include "GL/glew.h"
#else	// !_WIN32
#include <OpenGL/gl3.h>
#endif	// _WIN32


#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// these are the colour targets for OpenGL
const unsigned int egpfwTargetName[] = {
	GL_COLOR_ATTACHMENT0,	GL_COLOR_ATTACHMENT1,	GL_COLOR_ATTACHMENT2,	GL_COLOR_ATTACHMENT3,
	GL_COLOR_ATTACHMENT4,	GL_COLOR_ATTACHMENT5,	GL_COLOR_ATTACHMENT6,	GL_COLOR_ATTACHMENT7,
	GL_COLOR_ATTACHMENT8,	GL_COLOR_ATTACHMENT9,	GL_COLOR_ATTACHMENT10,	GL_COLOR_ATTACHMENT11,
	GL_COLOR_ATTACHMENT12,	GL_COLOR_ATTACHMENT13,	GL_COLOR_ATTACHMENT14,	GL_COLOR_ATTACHMENT15,
};

// color formats
const unsigned int egpfwInternalColorFormat[] = {
	0, GL_RGB8, GL_RGB16, GL_RGB32F, GL_RGBA8, GL_RGBA16, GL_RGBA32F,
};

// color storage type
const unsigned int egpfwInternalColorStorage[] = {
	0, GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT, GL_FLOAT, GL_UNSIGNED_BYTE, GL_UNSIGNED_SHORT, GL_FLOAT
};

// depth formats
const unsigned int egpfwInternalDepthFormat[] = {
	0, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT32F, GL_DEPTH24_STENCIL8,
};

// depth storage type
const unsigned int egpfwInternalDepthStorage[] = {
	0, GL_UNSIGNED_SHORT, GL_UNSIGNED_INT, GL_FLOAT, GL_UNSIGNED_INT
};


//-----------------------------------------------------------------------------

// ****
egpFrameBufferObjectDescriptor egpfwCreateFBO(
	const unsigned int frameWidth, const unsigned int frameHeight,
	const unsigned int numColorTargets, const egpColorFormat colorFormat,
	const egpDepthFormat depthFormat, const egpWrapSmoothFormat wrapSmoothFormat)
{
	egpFrameBufferObjectDescriptor fbo = { 0 };

	//generate FBO
	glGenFramebuffers(1, &fbo.glhandle);
	if (fbo.glhandle)
	{
		//bind FBO
		glBindFramebuffer(GL_FRAMEBUFFER, fbo.glhandle);

		//generate textures for FBO
		if (numColorTargets && colorFormat)
		{
			//check how many we can have
			glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, (int *)(&fbo.numColorTargets));
			if (fbo.numColorTargets > numColorTargets)
				fbo.numColorTargets = numColorTargets;

			// new textures
			glGenTextures(fbo.numColorTargets, fbo.colorTargetHandle);
			for (unsigned int i = 0, attach; i < fbo.numColorTargets; ++i)
			{
				// bind current and alloc space
				glBindTexture(GL_TEXTURE_2D, fbo.colorTargetHandle[i]);;
				glTexImage2D(GL_TEXTURE_2D, 0, egpfwInternalColorFormat[colorFormat],
					frameWidth, frameHeight, 0, GL_RGBA, egpfwInternalColorStorage[colorFormat], 0);

				//configure texture
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// texture gets small/large, smooth
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);		// texture repeats on horiz axis
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

				//associate textures with FBO
				attach = GL_COLOR_ATTACHMENT0 + i;
				glFramebufferTexture2D(GL_FRAMEBUFFER, attach, GL_TEXTURE_2D, fbo.colorTargetHandle[i], 0);
			}
		}
		else
		{
			fbo.numColorTargets = 0;
		}

		if (depthFormat)
		{
			const int format = depthFormat == DEPTH_D24S8 ? GL_DEPTH_STENCIL : GL_DEPTH_COMPONENT;
			const int attach = depthFormat == DEPTH_D24S8 ? GL_DEPTH_STENCIL_ATTACHMENT : GL_DEPTH_ATTACHMENT;

			// new textures	
			glGenTextures(1, fbo.depthTargetHandle);

			// bind current and alloc space
			glBindTexture(GL_TEXTURE_2D, fbo.depthTargetHandle[0]);;
			glTexImage2D(GL_TEXTURE_2D, 0, egpfwInternalDepthFormat[depthFormat],
				frameWidth, frameHeight, 0, format, egpfwInternalDepthStorage[depthFormat], 0);

			//configure texture
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// texture gets small/large, smooth
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);		// texture repeats on horiz axis
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			//associate textures with FBO
			glFramebufferTexture2D(GL_FRAMEBUFFER, attach, GL_TEXTURE_2D, fbo.depthTargetHandle[0], 0);
			fbo.hasDepthTarget = 1;
			if (depthFormat == DEPTH_D24S8)
				fbo.hasStencilTarget = 1;
		}
		else
		{
			fbo.hasDepthTarget = fbo.hasStencilTarget = 0;
		}

		//check status
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
		{
			fbo.frameWidth = frameWidth;
			fbo.frameHeight = frameHeight;
		}
		else
		{
			// destroy it
			egpfwReleaseFBO(&fbo);
		}

		//salt and salsa
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	return fbo;
}

// ****
void egpfwActivateFBO(const egpFrameBufferObjectDescriptor *fbo)
{
	if (fbo && fbo->glhandle)
	{
		//bind FBO
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->glhandle);

		//change draw buffers
		if (fbo->numColorTargets)
			glDrawBuffers(fbo->numColorTargets, egpfwTargetName);
		else
			glDrawBuffer(GL_NONE);

		//enable / disable depth
		if (fbo->hasDepthTarget)
		{
			glEnable(GL_DEPTH_TEST);
			if (fbo->hasStencilTarget)
				glEnable(GL_STENCIL_TEST);
		}
		else
		{
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_STENCIL);
		}

		// viewport
		glViewport(0, 0, fbo->frameWidth, fbo->frameHeight);
	}
	else
	{
		//unbind FBO
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

// ****
int egpfwReleaseFBO(egpFrameBufferObjectDescriptor *fbo)
{
	if (fbo && fbo->glhandle)
	{
		//turn off frame buffer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDeleteFramebuffers(1, &fbo->glhandle);
		if (fbo->numColorTargets)
			glDeleteTextures(fbo->numColorTargets, fbo->colorTargetHandle);
		if (fbo->hasDepthTarget)
			glDeleteTextures(1, fbo->depthTargetHandle);

		memset(fbo, 0, sizeof(egpFrameBufferObjectDescriptor));

		return 1;
	}

	return 0;
}


// ****
int egpfwBindColorTargetTexture(const egpFrameBufferObjectDescriptor *fbo, const unsigned int glBinding, const unsigned int targetIndex)
{
	if (fbo && fbo->glhandle && targetIndex < fbo->numColorTargets)
	{
		glActiveTexture(GL_TEXTURE0 + glBinding);
		glBindTexture(GL_TEXTURE_2D, fbo->colorTargetHandle[targetIndex]);
		return 1;
	}
	return 0;
}

// ****
int egpfwBindDepthTargetTexture(const egpFrameBufferObjectDescriptor *fbo, const unsigned int glBinding)
{
	if (fbo && fbo->glhandle)
	{
		glActiveTexture(GL_TEXTURE0 + glBinding);
		glBindTexture(GL_TEXTURE_2D, fbo->depthTargetHandle[0]);
		return 1;
	}
	return 0;
}