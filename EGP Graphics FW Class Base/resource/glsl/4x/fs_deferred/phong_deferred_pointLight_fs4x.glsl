/*
	Phong Deferred - Point light
	By Dan Buckstein
	Fragment shader that performs Phong shading for a point light in a deferred 
		rendering pipeline.
	
	Modified by: ______________________________________________________________
*/

// version
#version 410


// ****
// varyings
in vec4 position_clip;

// ****
// uniforms
uniform vec4 lightColor;
uniform vec4 lightPos;
uniform vec4 eyePos;

// ****
// target
layout (location = 0) out vec4 diffuse;
layout (location = 1) out vec4 specular;

// shader function
void main()
{
	// ****
	// since we did not generate this fragment using FSQ, 
	//	need to figure out where we are in screen space...
	vec4 ndc = position_clip / position_clip.w;
	vec4 screen = ndc * .5 + .5;

	// ****
	// output: calculate lighting for this light
	diffuse = vec4(screen.xy, 0.0, 0.0);

	specular = lightColor;
}