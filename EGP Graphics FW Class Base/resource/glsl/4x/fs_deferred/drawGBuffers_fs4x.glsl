/*
	Draw G-Buffers
	By Dan Buckstein
	Fragment shader that outputs incoming geometric attributes as color.
	
	Modified by: ______________________________________________________________
*/

// version
#version 410


// ****
// varyings
in vertexdata
{
	vec4 position_world;
	vec4 normal_world;
	vec4 texcoord;
} pass;

// ****
// target
layout (location = 0) out vec4 gbuffer_position;
layout (location = 1) out vec4 normal_position;
layout (location = 2) out vec4 texcoords_position;


// shader function
void main()
{
	// ****
	// copy inbound values to their respective g-buffer
	gbuffer_position = pass.position_world;
	normal_position = pass.normal_world;
	texcoords_position = pass.texcoord;

//DEBUGGING
	//normal_position = normalize(normal_position) * .5 + .5;
}