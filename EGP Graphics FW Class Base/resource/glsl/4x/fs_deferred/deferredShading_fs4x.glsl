/*
	Deferred Shading
	By Dan Buckstein
	Fragment shader that performs shading using geometry buffers.
	
	Modified by: ______________________________________________________________
*/

// version
#version 410

const vec3 ka = vec3(0.1, 0.1, 0.1);

// ****
// varyings
in vec2 passUV;

// ****
// uniforms
uniform sampler2D img_position;
uniform sampler2D img_normal;
uniform sampler2D img_texcoord;
uniform sampler2D tex_dm;
uniform sampler2D tex_sm;

#define NUM_LIGHTS 4
uniform vec4 lightColor[NUM_LIGHTS];
uniform vec4 lightPos[NUM_LIGHTS];
uniform vec4 eyePos;

// target
layout (location = 0) out vec4 fragColor;

void phong(in vec4 n, in vec4 p, in vec4 v,
			in vec4 lp, in vec4 lc, 
			inout vec4 diffuse, inout vec4 specular)
{
	vec4 l = normalize(lp - p);
	float kd = dot(n, l);
	kd = max(0, kd);
	diffuse += kd;
	
	vec4 r = (kd + kd)*n-l;
	float ks = dot(v, r);
	ks = max(0, ks);

	ks*=ks;
	ks*=ks;
	ks*=ks;
	ks*=ks;
	specular += ks;
}

// shader function
void main()
{
	// ****
	// output: calculate lighting for each light by reading in 
	//	attribute data from g-buffers

	vec4 position = texture(img_position, passUV);
	vec4 normal = texture(img_normal, passUV);
	vec4 texcoord = texture(img_texcoord, passUV);

	vec4 diffuseColor = texture(tex_dm, texcoord.xy);
	vec4 specularColor = texture(tex_sm, texcoord.xy);

	vec4 diffuseTotal = vec4(0.0),
		specularTotal = diffuseTotal;

	vec4 N = normalize(normal);
	vec4 L = normalize(lightPos[0] - position);
	vec4 V = normalize(eyePos - position);
	
	phong(N, position, V,
			lightPos[0], lightColor[0], 
			diffuseTotal, specularTotal);

	phong(N, position, V,
			lightPos[1], lightColor[1], 
			diffuseTotal, specularTotal);
			
	phong(N, position,
	55 V,
			lightPos[2], lightColor[2], 
			diffuseTotal, specularTotal);

	phong(N, position, V,
			lightPos[3], lightColor[3], 
			diffuseTotal, specularTotal);

	fragColor = diffuseTotal * diffuseColor + 
				specularTotal * specularColor +
				vec4(0.2, 0.1, 0.0, 0.0);

}
