/*
	Phong with Shadow Mapping
	By Dan Buckstein
	Fragment shader that performs Phong shading and shadow mapping.
	
	Modified by: ______________________________________________________________
*/

#version 410


// ****
// varyings: 
in vec4 position_clip_alt;
in vertex_lighting
{
	vec4 normal;
	vec4 texcoord;
	vec4 lightVec;
	vec4 eyeVec;
} pass;

// ****
// uniforms: 
uniform sampler2D shadowmap;
uniform sampler2D tex_dm;
uniform sampler2D tex_sm;

// target: 
layout (location = 0) out vec4 fragColor;


// ****
// constants/globals: 


void main()
{
	vec4 projector_screen = position_clip_alt / position_clip_alt.w;

	vec4 shadowSample = texture(shadowmap, projector_screen.xy);

	float shadowValue = shadowSample.r;
	float shadowDist = projector_screen.z;

	bool SHADOW = shadowDist > shadowValue + 0.001;

	vec4 diffuseSample = texture(tex_dm, pass.texcoord.xy);
	vec4 specularSample = texture(tex_sm, pass.texcoord.xy);

	fragColor = diffuseSample * (SHADOW ? 0.25 : 1.0);

	//fragColor = vec4(SHADOW ? 0.25 : 1.0);

	//fragColor = vec4(0.0, 0.5, 1.0, 1.0);
	//fragColor = shadowSample;
	//fragColor = vec4(shadowDist);
	//fragColor = vec4(shadowValue);
	//fragColor = normalize(pass.normal)*0.5 + 0.5;
	//fragColor = normalize(pass.lightVec)*0.5 + 0.5;
	//fragColor = normalize(pass.eyeVec)*0.5 + 0.5;
	//fragColor = pass.texcoord;
	//fragColor = diffuseSample;
	//fragColor = specularSample;
}