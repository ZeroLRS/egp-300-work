/*
	Phong (VS)
	By Dan Buckstein
	Vertex shader that passes data required to perform Phong shading.
	
	Modified by: Lucas Spiker, Duncan Carroll, and Gabriel Pereyra
*/

// version
#version 410

// attributes
layout (location = 0) in vec4 position;
layout (location = 8) in vec4 texcoord;
layout (location = 2) in vec4 normal;

// uniforms
uniform mat4 mvp;
uniform vec4 lightPos;
uniform vec4 eyePos;

// varyings
out vec2 passUV;
out vec4 passNormal;
out vec4 lightVec;
out vec4 viewerVec;

// shader function
void main()
{
	// set clip position
	gl_Position = mvp * position;

	// pass data
	passUV = texcoord.xy;
	passNormal = vec4(normal.xyz, 0.0);
	lightVec = lightPos - position;
	viewerVec = eyePos - position;
}