/*
	Morphing
	By Dan Buckstein
	Vertex shader that performs morph target animation.
	
	Modified by: ______________________________________________________________
*/

#version 410


// ****
// attributes: one per morphing attribute (e.g. multiple positions)
layout (location = 0) in vec4 p0;
layout (location = 1) in vec4 p1;
layout (location = 2) in vec4 p2;
layout (location = 3) in vec4 p3;

// ****
// uniforms: animation controls
uniform mat4 mvp;
uniform float param;
#define KEYFRAME_COUNT 4
uniform int index[KEYFRAME_COUNT];

// varying output: solid color
out vec4 passColor;

// ****
// LERP
vec4 lerp(in vec4 p0, in vec4 p1, const float t)
{

	return (p0 + (p1-p0)*t);

}

// ****
// Bezier curve interpolation
// easier with recursion... but that's not an option here...
//	...so we'll have to implement specific cases
vec4 sampleBezier0(in vec4 p0, const float t)
{
	return p0;
}
vec4 sampleBezier1(in vec4 p0, in vec4 p1, const float t)
{
	return lerp(
		sampleBezier0(p0, t),
		sampleBezier0(p1, t),
		t);
}
vec4 sampleBezier2(in vec4 p0, in vec4 p1, in vec4 p2, const float t)
{
	return lerp(
		sampleBezier1(p0, p1, t),
		sampleBezier1(p1, p2, t),
		t);
}
vec4 sampleBezier3(in vec4 p0, in vec4 p1, in vec4 p2, in vec4 p3, const float t)
{
	return lerp(
		sampleBezier2(p0, p1, p2, t),
		sampleBezier2(p1, p2, p3, t),
		t);
}

// ****
// Catmull-Rom spline interpolation
vec4 sampleCatmullRom(in vec4 pPrev, in vec4 p0, in vec4 p1, in vec4 pNext, const float t)
{
	const mat4 kernel = mat4(
		0, -1, 2, -1, 
		2, 0, -5, 3,
		0, 1, 4, -3,
		0, 0, -1, 1) * .5;

	mat4 influence = mat4(pPrev, p0, p1, pNext);

	vec4 time = vec4(1.0, t, t*t, t*t*t);

	return (influence * kernel * time);
}

// ****
// cubic Hermite spline interpolation
vec4 sampleCubicHermite(in vec4 p0, in vec4 m0, in vec4 p1, in vec4 m1, const float t)
{
	return p0;
}

vec4 morphLerp(
	in vec4 p[KEYFRAME_COUNT],
	in int i[KEYFRAME_COUNT])
{
	vec4 p0 = p[i[0]];
	vec4 p1 = p[i[1]];
	return lerp(p0, p1, param);
}

vec4 morphPingPong(
	in vec4 p[KEYFRAME_COUNT],
	in int i[KEYFRAME_COUNT])
{
	vec4 p0 = p[i[0]/2];
	vec4 p1 = p[i[1]/2];
	return lerp(p0, p1, param);
}

vec4 morphLerpReverse(
	in vec4 p[KEYFRAME_COUNT],
	in int i[KEYFRAME_COUNT])
{
	vec4 p0 = p[KEYFRAME_COUNT - 1 - i[0]];
	vec4 p1 = p[KEYFRAME_COUNT - 1 - i[1]];
	return lerp(p0, p1, param);
}

vec4 morphCatmullRom(
	in vec4 p[KEYFRAME_COUNT],
	in int i[KEYFRAME_COUNT])
{
	vec4 pprev = p[i[3]];
	vec4 p0 = p[i[0]];
	vec4 p1 = p[i[1]];
	vec4 pnext = p[i[2]];

	return sampleCatmullRom(pprev, p0, p1, pnext, param);
}

vec4 morphCubicHermite(
	in vec4 p[KEYFRAME_COUNT],
	in int i[KEYFRAME_COUNT])
{
	vec4 p0 = p[i[0]];
	vec4 p1 = p[i[1]] - p0;
	vec4 p2 = p[i[2]];
	vec4 p3 = p[i[3]] - p1;
	return sampleCubicHermite(p0, p1, p2, p3, param);
}

void main()
{
	// ****
	vec4 position;
	vec4 p[KEYFRAME_COUNT];
	p[0] = p0;
	p[1] = p1;
	p[2] = p2;
	p[3] = p3;

	position = morphCatmullRom(p, index);

	// do morphing, transform the correct result before assigning to output
	gl_Position = mvp * position;

	// TESTING: send position as color to give us some variance
	passColor = position*0.5+0.5;
}