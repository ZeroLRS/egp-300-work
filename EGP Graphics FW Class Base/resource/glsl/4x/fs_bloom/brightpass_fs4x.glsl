/*
	Bright Pass
	By Dan Buckstein
	Fragment shader that leaves bright pixels bright and makes dark darker.
	
	Modified by: ______________________________________________________________
*/

// version
#version 410


// varyings
in vec2 passUV;


// uniforms
uniform sampler2D img;


// target
layout (location = 0) out vec4 fragColor;


// shader function
void main()
{
	// ****
	// output: make bright values brighter, make dark values darker
	vec4 imgSample = texture(img, passUV);

	// luminance
	//float luminance = (imgSample.r + imgSample.g + imgSample.b) / 3.0;

	// relative luminance
	float luminance = 0.2126*imgSample.r + 0.7152*imgSample.g + 0.0722*imgSample.b;
	luminance *= luminance;
	luminance *= luminance;
	luminance *= luminance;
	luminance *= luminance;
	
	fragColor = luminance * imgSample;
}